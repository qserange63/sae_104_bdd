DROP TABLE FOURNIR;
DROP TABLE FOURNISSEUR;
DROP TABLE CONTENIR;
DROP TABLE PRODUIT;
DROP TABLE FACTURATION;
DROP TABLE ETATLOCATION;
DROP TABLE LOCATIONC;
DROP TABLE CLIENT;



CREATE TABLE CLIENT
(
    idClient char(4),
    nom varchar(20) NOT NULL,
    prenom varchar(20) NOT NULL,
    numTelephone char(10) NOT NULL,
    adresseClient varchar(50),
    mailClient varchar(60),
    PRIMARY KEY(idClient),
    CHECK(numTelephone LIKE('[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
);

CREATE TABLE LOCATIONC
(
    idLocation char(4),
    nom varchar(20) NOT NULL,
    dateLocation date,
    dateFinLocation date,
    idLocation char(4),
    idClient char(4),
    PRIMARY KEY(idLocation),
    FOREIGN KEY(idClient) REFERENCES CLIENT(idClient),
    FOREIGN KEY(idEtat) REFERENCES ETATLOCATION(idEtat),
    CHECK(dateLocation < dateFinLocation AND dateFinLocation-dateLocation < 15)
);

CREATE TABLE ETATLOCATION
(
    idEtat char(4),
    etat varchar(50) NOT NULL,
    usure varchar(20) NOT NULL,
    CHECK(etat IN ('réservé', 'loué', 'retourné') AND usure IN ('neuf', 'peu usé', 'usé', 'endommagé'))
);


CREATE TABLE FACTURATION(
    idFacture char(4),
    prix numeric(8,2),
    dateReglement date, 
    etatReglement varchar(20),
    idClient char(4),
    idLocation char(4),
    PRIMARY KEY(idFacture),
    FOREIGN KEY(idClient) REFERENCES CLIENT(idClient),
    FOREIGN KEY(idLocation) REFERENCES LOCATIONC(idLocation),
    CHECK(etatReglement IN ('réglée', 'en cours', 'retard paiement') AND CURRENT_DATE - dateReglement < 30)
);

CREATE TABLE PRODUIT
(
    idProduit char(4),
    nomProduit varchar(20),
    marqueProduit varchar(20),
    taille varchar(4),
    usureProduit varchar(20),
    caution numeric(8,2),
    typeProduit varchar(20),
    idFournisseur char(4),
    PRIMARY KEY(idProduit),
    CHECK(usureProduit IN('neuf', 'peu usé', 'usé', 'endommagé') AND taille IN ('XS', 'M', 'L', 'XL', 'XXL'))
);

CREATE TABLE CONTENIR
(
    idLocation char(4),
    idProduit char(4),
    PRIMARY KEY(idLocation,idProduit),
    FOREIGN KEY (idLocation) REFERENCES LOCATIONC(idLocation),
    FOREIGN KEY (idProduit) REFERENCES PRODUIT(idProduit)
);

CREATE TABLE FOURNISSEUR
(
    idFournisseur char(4),
    nom varchar(20),
    provenance varchar(50),
    dateLivraison date,
    PRIMARY KEY(idFournisseur)
);

CREATE TABLE FOURNIR
(
    idFournisseur char(4),
    idProduit char(4),
    PRIMARY KEY (idFournisseur, idProduit),
    FOREIGN KEY (idFournisseur) REFERENCES FOURNISSEUR(idFournisseur);
    FOREIGN KEY (idProduit) REFERENCES PRODUIT(idProduit)
);


INSERT INTO CLIENT VALUES ('COO1','Herta','Jean','0689988776','4 rue du Jambon','Hertajean@gmail.com');
INSERT INTO CLIENT VALUES ('COO2','Nutell','Amandine','0783167332','14 Boulevard du Chocolat','Nutell.Amandine@gmail.com');
INSERT INTO CLIENT VALUES ('COO3','Lipton','Heins','0765654398','65 rue Préhistorique','Lipton.Heins@gmail.com');
INSERT INTO CLIENT VALUES ('COO4','Origina','Louise','0676927302','7 avenue Brouillon','Origina.Louise@gmail.com');

INSERT INTO LOCATIONC VALUES ('L001','Location Ecole','SEP-13-2022','SEP-20-2022','C001');
INSERT INTO LOCATIONC VALUES ('L002','Location Ecole','OCT-15-2022','DEC-20-2022','C002');
INSERT INTO LOCATIONC VALUES ('L003','Location Ecole','JAN-10-2022','JAN-20-2022','C003');

INSERT INTO ETATLOCATION VALUES ('E001','réservé','neuf');
INSERT INTO ETATLOCATION VALUES ('E002','loué','usé');
INSERT INTO ETATLOCATION VALUES ('E003','loué','peu usé');
INSERT INTO ETATLOCATION VALUES ('E004','retourné','endommagé');

INSERT INTO FACTURATION VALUES ('Z001',10.00,'SEP-15-2022','réglée','C001','L001');
INSERT INTO FACTURATION VALUES ('Z002',1000.00,'OCT-30-2022','en cours','C002','L002');
INSERT INTO FACTURATION VALUES ('Z003',150.00,'JAN-20-2022','retard paiement','C003','L003');

INSERT INTO PRODUIT('P001','Raquette','SkyCo',42,'neuf','XL','Ski');
INSERT INTO PRODUIT('P002','Bonnet','IKEO',40,'usé','L','Accessoirei');
INSERT INTO PRODUIT('P003','Gants','Switch',34,'endommagé','S','Accessoire');
INSERT INTO PRODUIT('P004','Baton Ski','Lonovo',181,'neuf','M','Baton SKi');

INSERT INTO CONTENIR VALUES ('L001','P001');
INSERT INTO CONTENIR VALUES ('L002','P002');
INSERT INTO CONTENIR VALUES ('L003','P003');

INSERT INTO FOURNISSEUR VALUES ('F001','SkyCo','Finlande','SEP-15-2022');
INSERT INTO FOURNISSEUR VALUES ('F002','IKEO','Suède','JAN-10-2022');
INSERT INTO FOURNISSEUR VALUES ('F003','Switch','Irlande','OCT-20-2022');
INSERT INTO FOURNISSEUR VALUES ('F004','Lonovo','Allemagne','SEP-30-2022');

INSERT INTO FOURNIR VALUES ('F001','P001');
INSERT INTO FOURNIR VALUES ('F002','P002');
INSERT INTO FOURNIR VALUES ('F003','P003');
INSERT INTO FOURNIR VALUES ('F004','P004');


